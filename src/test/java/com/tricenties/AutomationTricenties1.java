package com.tricenties;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class AutomationTricenties1 {
	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/login");
		driver.manage().window().maximize();
		driver.findElement(By.id("Email")).sendKeys("pdkumar241095@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Dilipkd#2124");
		driver.findElement(By.xpath("(//input[@type='submit'])[2]")).click();
		WebElement computer = driver.findElement(By.xpath("(//a[contains(text(),'Compute')])[1]"));
		Actions a = new Actions(driver);
		Thread.sleep(2000);
		a.moveToElement(computer).perform();
		WebElement desktop = driver.findElement(By.xpath("(//a[contains(text(),'Desktop')])[1]"));
		a.moveToElement(desktop).click().perform();
		Thread.sleep(2000);
		driver.findElement(By.id("products-orderby")).click();
		a.sendKeys(Keys.ARROW_DOWN).build().perform();
		Thread.sleep(1000);
		a.sendKeys(Keys.ARROW_DOWN).build().perform();
		Thread.sleep(1000);
		a.sendKeys(Keys.ARROW_DOWN).build().perform();
		Thread.sleep(1000);
		a.sendKeys(Keys.ENTER).build().perform();
		driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("add-to-cart-button-72")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//span[text()='Shopping cart']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='removefromcart']")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
		driver.findElement(By.xpath("(//input[@class='button-1 new-address-next-step-button'])[1]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@onclick='Shipping.save()']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@class='button-1 shipping-method-next-step-button']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@class='button-1 payment-method-next-step-button']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@class='button-1 payment-info-next-step-button']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@value='Confirm']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Click here for order details.']")).click();
		List<WebElement> orderlist = driver.findElements(By.xpath("//div[@class='page order-details-page']"));
		for (WebElement billpage : orderlist) {
			System.out.println(billpage.getText());

		}
		driver.findElement(By.xpath("(//a[text()='Log out'])[1]")).click();
		System.out.println("Successfully Logged out");
	}

}
